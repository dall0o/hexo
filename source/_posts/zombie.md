---
title: Zombie
---

Bonjour à tous,

Nous allons nous lancer dans une petite session filmographique sur les Morts-vivants.

[![Pretty!!!! by Hunter Bennett](https://farm3.staticflickr.com/2552/4057858219_3fc891c89c_o.jpg)](https://www.flickr.com/photos/bringinudownfromabove/4057858219/)

Le but est de revivre l'épopée morts-vivantes en une dizaine de film:

1. [Les Morts-vivants](https://fr.wikipedia.org/wiki/Les_Morts-vivants_(film,_1932)) (White Zombie) de [Victor Halperin](https://fr.wikipedia.org/wiki/Victor_Halperin), sorti en 1932
1. [Le Cadavre qui tue](https://fr.wikipedia.org/wiki/Le_Cadavre_qui_tue) (Doctor Blood's Coffin) de [Sidney J. Furie](https://fr.wikipedia.org/wiki/Sidney_J._Furie), sorti en 1961

1. [La Nuit des morts-vivants](https://fr.wikipedia.org/wiki/La_Nuit_des_morts-vivants "La Nuit des morts-vivants") (Night of the Living Dead) de [George A. Romero](https://fr.wikipedia.org/wiki/George_Andrew_Romero "George Andrew Romero"), sorti en 1968
1. [Zombie](https://fr.wikipedia.org/wiki/Zombie_(film) "Zombie (film)") (Dawn of the Dead) de [George A. Romero](https://fr.wikipedia.org/wiki/George_Andrew_Romero "George Andrew Romero"), sorti en 1978
1. [Le Jour des morts-vivants](https://fr.wikipedia.org/wiki/Le_Jour_des_morts-vivants "Le Jour des morts-vivants") (Day of the Dead) de [George A. Romero](https://fr.wikipedia.org/wiki/George_Andrew_Romero "George Andrew Romero"), sorti en 1985

1.  [L'Enfer des zombies](https://fr.wikipedia.org/wiki/L%27Enfer_des_zombies) (Zombi 2)*  de [Lucio Fulci](https://fr.wikipedia.org/wiki/Lucio_Fulci), sorti en 1979

1. [28 jours plus tard](https://fr.wikipedia.org/wiki/28_jours_plus_tard) (28 Days Later) de [Danny Boyle](https://en.wikipedia.org/wiki/Danny_Boyle), sorti en 2002
1. [28 semaines plus tard](https://fr.wikipedia.org/wiki/28_semaines_plus_tard) (28 Weeks Later) de [Juan Carlos Fresnadillo](https://fr.wikipedia.org/wiki/Juan_Carlos_Fresnadillo), sorti en 2007

1. [L'Armée des morts](https://fr.wikipedia.org/wiki/L%27Arm%C3%A9e_des_morts) (Dawn of the Dead)** de [Zack Snyder](https://fr.wikipedia.org/wiki/Zack_Snyder), sorti en 2004

1. [Le Territoire des morts](https://fr.wikipedia.org/wiki/Le_Territoire_des_morts) (Land of the Dead) de [George A. Romero](https://fr.wikipedia.org/wiki/George_Andrew_Romero "George Andrew Romero"), sorti en 2005
1. [Chronique des morts-vivants](https://fr.wikipedia.org/wiki/Chronique_des_morts-vivants) (Diary of the Dead) de [George A. Romero](https://fr.wikipedia.org/wiki/George_Andrew_Romero "George Andrew Romero"), sorti en 2008
1. [Le Vestige des morts-vivants](https://fr.wikipedia.org/wiki/Le_Vestige_des_morts-vivants) (Survival of the Dead) de [George A. Romero](https://fr.wikipedia.org/wiki/George_Andrew_Romero "George Andrew Romero"), sorti en 2010

1. [Warm Bodies](https://fr.wikipedia.org/wiki/Warm_Bodies) (Warm Bodies) de [Jonathan Levine](https://fr.wikipedia.org/wiki/Jonathan_Levine), sorti en 2013
1. [World War Z](https://fr.wikipedia.org/wiki/World_War_Z_%28film%29) (World War Z) de [Marc Forster](https://fr.wikipedia.org/wiki/Marc_Forster), sorti en 2013


*_Le titre international de Zombie 2 a mal été choisi, l'action se déroulant avant le Zombie (Dawn of the Dead) de George A. Romero. Fulci a renommé ce film pour profiter du succès de Zombie, sorti un an auparavant._

**_C'est un remake du film Zombie réalisé par George A. Romero et sorti en 1978. les prémisses et le lieu central sont les mêmes, mais l'histoire diffère._

Je n'ai pas intégré de parodie comme Shaun of the Dead or Zombieland. Shaun of the Dead sera vu une autre fois dans un marathon sur "La Trilogie du Cornetto".

---

Proposition

* [Re animator 1985](http://filmsdezombies.over-blog.com/article-re-animator-82263052.html)
* [The return of the living dead 1985](https://duckduckgo.com/?q=+The+Return+of+the+Living+Dead+1985&t=ffab&ia=web)
* [The Serpent and the Rainbow 1988](https://movieweb.com/movie/the-serpent-and-the-rainbow/)
* [Night of the creeps 1986](https://movieweb.com/movie/night-of-the-creeps/)
* [Planet Terror 2007](https://movieweb.com/movie/planet-terror/)
* [Braindead 1992](https://en.wikipedia.org/wiki/Braindead_%28film%29)